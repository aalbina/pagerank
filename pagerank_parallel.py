# coding: utf-8
import operator
from collections import defaultdict
from datetime import datetime
from joblib import Parallel, delayed


def count(k, set):
    temp = defaultdict(int)

    for j in set:
        temp[j] += k / len(set)
    return temp


sets = defaultdict(set)
links = {}

f = open("sets.txt", "r")
i = 0
for line in f:
    sets[i] = set([int(x) for x in line.strip().split(" ") if x])
    i += 1

f = open("links.txt", "r")
i = 0
for line in f:
    links[i] = line.strip()
    i += 1

# ***************************** #
now = datetime.now()
ranks = defaultdict(int)
d = 0.85
for i in range(len(sets)):
    ranks[i] = 1

for i in range(10):
    results = Parallel(n_jobs=5)(
        delayed(count)(ranks[i] * d, set) for i, set in sets.iteritems())

    for result in results:
        for i in range(len(sets)):
            ranks[i] += result[i]
print datetime.now() - now
# ***************************** #

ranks = sorted(ranks.items(), key=operator.itemgetter(1), reverse=True)
f = open("pr_parallel.txt", "w")
for i, set in ranks:
    if links[i]:
        f.write("{:<3} - {:^35}: {:.2f} \n".format(
            i, links[i], set
        ))
f.close()
