# coding: utf-8
import operator
from collections import defaultdict
from datetime import datetime


sets = defaultdict(set)
links = {}

f = open("sets.txt", "r")
i = 0
for line in f:
    sets[i] = set([int(x) for x in line.strip().split(" ") if x])
    i += 1

# for i, set in sets.iteritems():
#     print set

f = open("links.txt", "r")
i = 0
for line in f:
    links[i] = line.strip()
    i += 1

# for i, link in links.iteritems():
#     print link

# ***************************** #
now = datetime.now()
ranks = {}
d = 0.85
for i in range(len(sets)):
    ranks[i] = 1

for i in range(10):
    temp = defaultdict(int)
    for i, set in sets.iteritems():
       k = ranks[i] * d
       count = len(set)

       for j in set:
           temp[j] += k / count

    for i in range(len(sets)):
        ranks[i] += temp[i]
print datetime.now() - now
# ***************************** #

ranks = sorted(ranks.items(), key=operator.itemgetter(1), reverse=True)
f = open("pr.txt", "w")
for i, set in ranks:
    if links[i]:
        f.write("{:<3} - {:^35}: {:.2f} \n".format(
            i, links[i], set
        ))
f.close()
