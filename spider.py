# coding: utf-8
import urllib2

from collections import defaultdict
from BeautifulSoup import BeautifulSoup

# site settings
html_page = urllib2.urlopen("https://habrahabr.ru/post/321446/")
site_mask = "https://habrahabr.ru/post"
uncore = '#'

# base settings
soup = BeautifulSoup(html_page)
links = defaultdict(str)
links_refs = defaultdict(set)

links_num = 100
i = 0
j = 1
links[0] = "https://habrahabr.ru/post/321446/"
current_link = "https://habrahabr.ru/post/321446/"

# collecting pages
while len(links) < links_num:
    l = 0

    if len(links) > 1:
        current_link = links[i]
        if current_link:
            html_page = urllib2.urlopen(current_link)
            soup = BeautifulSoup(html_page)
        else:
            break

    for link in soup.findAll('a'):
        link_str = link.get('href')

        if link_str is not None:
            link_str = link_str.encode('ascii','ignore')
            if uncore not in link_str and link_str.startswith(site_mask):
                if link_str in links.values():
                    for k, value in links.iteritems():
                        if link_str == value:
                            break
                    links_refs[i].add(k)
                else:
                    links[j] = link.get('href')
                    links_refs[i].add(j)
                    j += 1
                l += 1

    i += 1

# checking
f = open("sets.txt", "w")
f2 = open("links.txt", "w")
for i in range(len(links)):
    print (i, links[i])
    print links_refs[i]
    for el in links_refs[i]:
        f.write("{} ".format(el))
    f.write("\n")
    f2.write("{}\n".format(links[i]))
f.close()
f2.close()

k = len(links)
print k

# print matrix
f = open("matrix.txt", "w")
for i in range(k):
    for j in range(k):
        if j in links_refs[i]:
            f.write("{} ".format(1))
        else:
            f.write("{} ".format(0))

    f.write("\n")
f.close()

